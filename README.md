# ARM-KL25Z-mBed-IDE-Example-Progrms

Basic Programming exercises for FRDM-KL25Z: Freedom Development Platform for Kinetis® KL14, KL15, KL24, KL25 MCUs. The Freedom KL25Z is an ultra-low-cost development platform for Kinetis® L Series KL1x (KL14/15) and KL2x (KL24/25) MCUs built on Arm®.